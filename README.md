# Docker-compose LEMP Kit

run to build at first
```
docker-compose up -d
```

then u can use
```
docker-compose stop
docker-compose start
```

dnsmasq 
-----
For comfortable work, I suggest you to use dnsmasq for automatic resolving of such domains as http://*.d/.  
for mac os - https://passingcuriosity.com/2013/dnsmasq-dev-osx/ (use .d or .localhost, DO NOT USE .dev or .local)



Xdebug - https://blog.denisbondar.com/post/phpstorm_docker_xdebug

```
lsof -nP -i4TCP:443 | grep LISTEN

lsof -i TCP:443

ps axu | grep 19476
```